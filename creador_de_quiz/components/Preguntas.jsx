const { React, ReactDOM } = window;

class Preguntas extends React.Component {
  constructor(props) {
    super(props);
    let { match } = this.props;
    let id = match.params.id;
    var index = 0;
    switch (id) {
      case "Ciencias":
        index = 0;
        break;
      case "Historia":
        index = 1;
        break;
      case "Deportes":
        index = 2;
        break;
      case "Geografia":
        index = 3;
        break;
    }
    this.state = { index: index, pregunta_index: 0, preguntas: [] };
    this.responder = this.responder.bind(this);
  }

  componentDidMount() {
    let { match } = this.props;
    let id = match.params.id;

    const operationName = null;
    const variables = {};
    const query = `{
      obtenerPreguntas(tipo: "${id}"){
        tipo
        pregunta
        respuestas{
          res
          correcta
        }
      }
    }`;

    const body = { operationName, variables, query };

    fetch("http://localhost:4000", {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(res => res.json())
      .then(res => {
        const copia = [...this.state.preguntas];
        copia[this.state.index] = {
          id,
          preguntas: res.data.obtenerPreguntas
        };
        this.setState({
          preguntas: copia
        });
      });
  }

  responder(event) {
    let respuesta = event.currentTarget.innerHTML;
    console.log(respuesta);
    var correcta = "";
    const index_pregunta = this.state.pregunta_index;
    const categoria = this.state.preguntas[this.state.index];
    const pregunta_actual = categoria.preguntas[this.state.pregunta_index];
    pregunta_actual.respuestas.map(elem => {
      if (elem.correcta) {
        correcta = elem.res;
      }
      if (elem.res === respuesta) {
        if (elem.correcta) {
          alert("EXACTO, eres un genio, espera a la siguiente pregunta");
        } else {
          alert(
            `la respuesta correcta es: ${correcta}, Espera a la siguiente pregunta`
          );
        }
      }
    });
    console.log(pregunta_actual.respuestas.length);
    setTimeout(() => {
      if (index_pregunta + 1 >= pregunta_actual.respuestas.length - 1) {
        console.log("ERROR");
        this.setState({ pregunta_index: 0 });
      } else {
        console.log(index_pregunta);
        this.setState({ pregunta_index: this.state.pregunta_index + 1 });
      }
    }, 1000);
  }
  render() {
    const preguntas = this.state.preguntas;

    if (preguntas.length === 0) {
      return null;
    }

    return (
      <div className="categorias">
        <h2>
          {
            preguntas[this.state.index].preguntas[this.state.pregunta_index]
              .pregunta
          }
        </h2>
        <div>
          <div className="preguntas-container">
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[0].res
              }
            </a>
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[1].res
              }
            </a>
          </div>
          <div className="preguntas-container">
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[2].res
              }
            </a>
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[3].res
              }
            </a>
          </div>
        </div>
      </div>
    );
  }
}
