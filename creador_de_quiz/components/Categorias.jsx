const { React, ReactDOM } = window;
const { Link } = window.ReactRouterDOM;

class Categorias extends React.Component {
  constructor() {
    super();
    this.state = {
      categorias: []
    };
  }

  componentDidMount() {
    const query = `{
      obtenerCategorias{
        name
        link
      }
    }
    `;
    const body = { operationName: null, variables: {}, query };

    fetch("http://localhost:4000", {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(res => res.json())
      .then(res => this.setState({ categorias: res.data.obtenerCategorias }));
  }

  render() {
    return this.state.categorias.map(elem => (
      <div className="categorias" key={elem.name}>
        <img src={elem.link}></img>
        <br />
        <Link to={`/preguntas/${elem.name}`}>
          <p>{elem.name}</p>
        </Link>
      </div>
    ));
  }
}
