const { React, ReactDOM } = window;
class HasPregunta extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tipo: "Ciencias",
      pregunta: "",
      res1: { res: "", correcta: false },
      res2: { res: "", correcta: false },
      res3: { res: "", correcta: false },
      res4: { res: "", correcta: false },
    };

    this.getSelectedValue = this.getSelectedValue.bind(this);
    this.getPregunta = this.getPregunta.bind(this);
    this.getRes = this.getRes.bind(this);
    this.getRightAnswer = this.getRightAnswer.bind(this);
    this.enviarPregunta = this.enviarPregunta.bind(this);
  }
  getSelectedValue(event) {
    this.setState({ tipo: event.target.value });
  }
  getPregunta(event) {
    this.setState({ pregunta: event.target.value });
  }

  getRes(event) {
    const { name, value } = event.target;

    this.setState((prevState) => ({
      [name]: {
        ...prevState[name],
        res: value,
      },
    }));
  }

  getRightAnswer(event) {
    const { name, checked } = event.target;

    this.setState((prevState) => ({
      [name]: {
        ...prevState[name],
        correcta: checked,
      },
    }));
  }

  enviarPregunta() {
    const { tipo, pregunta, ...respuestas } = this.state;

    const formulario = {
      tipo,
      pregunta,
      respuestas: Object.values(respuestas),
    };

    const operationName = "crearPregunta";
    const variables = { input: formulario };
    const query = `mutation crearPregunta($input: InputCrearPregunta!){
      crearPregunta(input: $input){
        tipo
        pregunta
        respuestas{
          res
          correcta
        }
      }
    }`;
    const body = { operationName, variables, query };

    fetch("http://localhost:4000", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.errors) {
          alert(
            "La pregunta no se pudo crear ☹. Inténtalo de nuevo."
          );
        } else alert("La pregunta se creó exitosamente 😁");
      })
      .catch("La pregunta no se pudo crear ☹");
  }

  render() {
    return (
      <form>
        <div className="categorias">
          <p>Llena los campos para poder agregar una pregunta</p>
          <select
            name="categorias"
            onChange={this.getSelectedValue}
            value={this.state.tipo}
          >
            <option value="Ciencias">Ciencias</option>
            <option value="Historia">Historia</option>
            <option value="Deportes">Deportes</option>
            <option value="Geografia">Geografia</option>
          </select>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Pregunta"
            onChange={this.getPregunta}
          ></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 1"
            name="res1"
            onChange={this.getRes}
          ></input>
          <input
            type="checkbox"
            id="cbox1"
            name="res1"
            onChange={this.getRightAnswer}
          ></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 2"
            name="res2"
            onChange={this.getRes}
          ></input>
          <input
            type="checkbox"
            id="cbox2"
            name="res2"
            onChange={this.getRightAnswer}
          ></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 3"
            name="res3"
            onChange={this.getRes}
          ></input>
          <input
            type="checkbox"
            id="cbox3"
            name="res3"
            onChange={this.getRightAnswer}
          ></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 4s"
            name="res4"
            onChange={this.getRes}
          ></input>
          <input
            type="checkbox"
            id="cbox4"
            name="res4"
            onChange={this.getRightAnswer}
          ></input>
          <br />
          <br />
          <a className="inp btn-answer" onClick={this.enviarPregunta}>
            Enviar pregunta
          </a>
        </div>
      </form>
    );
  }
}
