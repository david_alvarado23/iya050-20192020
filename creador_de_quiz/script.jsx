const { React, ReactDOM, App } = window;

ReactDOM.render(
  <div>
    <App />
  </div>,
  document.getElementById("root")
);
