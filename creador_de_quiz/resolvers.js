const fetch = require("node-fetch");
const apiUrl =
  "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs/";
const appId = "preguntas-creador-quiz-pre";
const headers = {
  "x-application-id": "david.alvarado",
  "Content-Type": "application/json",
};

/**
 * Devuelve la clave para cada categoria y sus preguntas.
 * @param {string} tipo - Categoria
 * @param {number} subtipo - Id de la pregunta de la categoria
 */
const llavePreguntas = (tipo, subtipo) =>
  !subtipo ? `${appId}-${tipo}` : `${appId}-${tipo}-${subtipo}`;

/**
 * Obtiene las categorias de una clave estatica
 */
const obtenerCategorias = async () => {
  const key = "preguntas-creador-quiz-categorias";

  try {
    const respuesta = await fetch(`${apiUrl}${key}`, {
      method: "get",
      headers: { ...headers },
    });

    if (respuesta.status === 200) {
      const json = await respuesta.json();
      return JSON.parse(json.value);
    }
  } catch (err) {
    console.error(err);
  }
};

/**
 * Obtiene los ids de las preguntas para una categoría en específico.
 * Devuelve un array vacio o un array de ids.
 * @param {string} tipo Categoria
 */
const obtenerIdsPreguntasAnteriores = async (tipo) => {
  const key = llavePreguntas(tipo);

  try {
    const respuesta = await fetch(`${apiUrl}${key}`, {
      method: "get",
      headers: { ...headers },
    });

    if (respuesta.status === 200) {
      try {
        const json = await respuesta.json();
        return JSON.parse(json.value);
      } catch (error) {
        // No habia ningun dato guardado en esta clave.
        return [];
      }
    }
  } catch (err) {
    console.error(err);
    throw new Error("No se pudieron obtener las preguntas anteriores");
  }
};

/**
 * Guarda la pregunta y sus respuestas para una categoría.
 * Devuelve la pregunta creada.
 * @param {string} tipo - Categoria
 * @param {number} id - Id de la nueva pregunta
 * @param {string} pregunta
 * @param {array} respuestas
 */
const guardarPregunta = async (tipo, id, pregunta, respuestas) => {
  const key = llavePreguntas(tipo, id);
  const body = { p: pregunta, r: respuestas };

  const respuesta = await fetch(`${apiUrl}${key}`, {
    method: "put",
    headers: { ...headers },
    body: JSON.stringify(body),
  });

  if (respuesta.status === 200) {
    const json = await respuesta.json();
    const jsonParseado = JSON.parse(json.value);
    return {
      tipo,
      pregunta: jsonParseado.p,
      respuestas: jsonParseado.r,
    };
  } else {
    throw new Error("Could not be saved");
  }
};

/**
 * Añade un id a la lista de ids de preguntas de la categoria.
 * @param {string} tipo Categoria
 * @param {array} idsAnteriores Array de ids actuales
 * @param {number} id Nuevo id a insertar
 */
const añadirIdACategoria = async (tipo, idsAnteriores, id) => {
  const key = llavePreguntas(tipo);
  const body = [...idsAnteriores, id];

  const respuesta = await fetch(`${apiUrl}${key}`, {
    method: "put",
    headers: { ...headers },
    body: JSON.stringify(body),
  });

  if (respuesta.status === 200) {
    const json = await respuesta.json();
    return JSON.parse(json.value);
  } else {
    throw new Error("Could not be saved into category");
  }
};

/**
 * Devuelve una pregunta de una categoría dado un id.
 * @param {string} tipo Categoria
 * @param {number} id Id de la pregunta a obtener
 */
const obtenerPregunta = async (tipo, id) => {
  const key = llavePreguntas(tipo, id);

  try {
    const respuesta = await fetch(`${apiUrl}${key}`, {
      method: "get",
      headers: { ...headers },
    });

    if (respuesta.status === 200) {
      const json = await respuesta.json();
      const jsonParseado = JSON.parse(json.value);
      return {
        tipo,
        pregunta: jsonParseado.p,
        respuestas: jsonParseado.r,
      };
    }
  } catch (err) {
    console.error(err);
    throw new Error("No se pudo obtener la pregunta", id);
  }
};

module.exports = {
  Query: {
    obtenerCategorias: async () => {
      return await obtenerCategorias();
    },
    obtenerPreguntas: async (_, { tipo }, ___) => {
      // Obtiene el array de ids de preungtas y luego hace fetch para cada uno de esos ids.
      const idsPreguntas = await obtenerIdsPreguntasAnteriores(tipo);
      if (idsPreguntas) {
        return await idsPreguntas.map(
          async (id) => await obtenerPregunta(tipo, id)
        );
      } else {
        throw new Error(
          `No se pudieron obtener las preguntas para la categoría ${tipo}`
        );
      }
    },
  },
  Mutation: {
    crearPregunta: async (_, { input }) => {
      const { tipo, pregunta, respuestas } = input;

      try {
        // Obtiene los ids de preguntas anteriores dentro de la categoria
        const idsAnteriores = await obtenerIdsPreguntasAnteriores(tipo);

        // Obtiene el ultimo de estos o 0 y le añade un (autoincremental)
        const ultimoId =
          idsAnteriores.length === 0 ? 0 : +idsAnteriores.slice(-1);
        const idNuevo = ultimoId + 1;

        const preguntaCreada = await guardarPregunta(
          tipo,
          idNuevo,
          pregunta,
          respuestas
        );

        // Añade el nuevo id al array de ids de preguntas de la categoría.
        // Si falla al insertar el id a la categoría se volvería a intentar.
        // Si no se puede insertar en esta segunda vez la pregunta estaría creada con un id,
        // pero este se sobreescribirá en la siguiente consulta.
        try {
          await añadirIdACategoria(tipo, idsAnteriores, idNuevo);
        } catch (err) {
          await añadirIdACategoria(tipo, idsAnteriores, idNuevo);
        }

        return preguntaCreada;
      } catch (err) {
        throw new Error("No se pudo crear la pregunta para esta categoría.");
      }
    },
  },
};
