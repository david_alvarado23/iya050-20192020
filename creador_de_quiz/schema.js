module.exports = `
    type Pregunta {
        tipo: String!
        pregunta: String!
        respuestas: [Respuesta!]!
    }

    type Respuesta {
        res: String!
        correcta: Boolean!
    }

    input InputRespuesta {
        res: String!
        correcta: Boolean!
    }

    input InputCrearPregunta {
        tipo: String!
        pregunta: String!
        respuestas: [InputRespuesta!]!
    }

    type Categoria {
        name: String!
        link: String
    }

    type Query {
        obtenerCategorias: [Categoria]
        obtenerPreguntas(tipo: String!): [Pregunta]
    }

    type Mutation {
        crearPregunta(input: InputCrearPregunta!): Pregunta
    }
`;
