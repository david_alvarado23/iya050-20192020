const { GraphQLServer } = require("graphql-yoga");
const typeDefs = require("./schema");
const resolvers = require("./resolvers");

const server = new GraphQLServer({ typeDefs, resolvers });
server.start(() => console.log("Running on http://localhost:4000"));
