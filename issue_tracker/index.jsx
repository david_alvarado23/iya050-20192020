const { React, ReactDOM, ReactRouterDOM, Home, Issue, CreateIssue } = window;
const { Switch, BrowserRouter: Router, Route, Link } = ReactRouterDOM;
const { useState, useEffect } = React;

const App = () => {
  const [issues, setIssues] = useState([]);
  const [status, setStatus] = useState([]);

  useEffect(() => {
    getIssues();
    getStatus();
  }, []);

  const getIssues = () => {
    fetch("http://localhost:8080/stub.json")
      .then(res => res.json())
      .then(parsedRes => parsedRes.issues)
      .then(setIssues);
  };

  const getStatus = () => {
    fetch("http://localhost:8080/stub.json")
      .then(res => res.json())
      .then(parsedRes => parsedRes.status)
      .then(setStatus);
  };

  const changeIssueStatus = (issueId, status) => {
    setIssues(issues => {
      const issueIndexToUpdate = issues.findIndex(
        issue => issue.id === issueId
      );

      return [
        ...issues.slice(0, issueIndexToUpdate),
        {
          ...issues[issueIndexToUpdate],
          status
        },
        ...issues.slice(issueIndexToUpdate + 1)
      ];
    });
  };

  return (
    <Router>
      <nav className="navBar">
        <Link to="/" className="navBarItem">
          Issue Tracker
        </Link>
      </nav>
      <Switch>
        <Route
          exact
          path="/"
          component={props => <Home {...props} issues={issues} />}
        />
        <Route
          exact
          path="/issue/:id"
          component={props => (
            <Issue
              {...props}
              changeStatus={changeIssueStatus}
              statusList={status}
              issues={issues}
            />
          )}
        />
        <Route exact path="/create/issue" component={CreateIssue} />
      </Switch>
    </Router>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
