const { React } = window;

(global => {
  /**
   * I set this component to wrap every 'input' of a form to add them a regular style through the app.
   * Presentational components.
   * @param {*} param0
   */
  const Input = ({ title, ...props }) => (
    <div className="formField">
      <label className="subHeader">{title}</label>
      <input className="formFieldAttribute" {...props} />
    </div>
  );

  const TextArea = ({ title, ...props }) => (
    <div className="formField">
      <label className="subHeader">{title}</label>
      <textarea className="formFieldAttribute" {...props} />
    </div>
  );

  const Select = ({ title, options, optionProps, ...props }) => (
    <div className="formField">
      <label className="subHeader">{title}</label>
      <select className="formFieldAttribute" {...props}>
        {options.map((option, i) => (
          <option
            key={`${title}.${option.id || i}`}
            value={option.id}
            {...(optionProps && optionProps(option))}
          >
            {option.value}
          </option>
        ))}
      </select>
    </div>
  );

  global.Input = Input;
  global.TextArea = TextArea;
  global.Select = Select;
})(window);
