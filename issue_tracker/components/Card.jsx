const { React } = window;

(global => {
  /**
   * A UI Card component. Used for the list of issues.
   * It has a header and a the content. The card can or not
   * be clickeable.
   * A Presentational component.
   * @param {*} props
   */
  const Card = props => {
    return (
      <div
        className={`card ${props.clickeable ? "clickeable" : ""}`}
        onClick={props.onClick}
      >
        <div className="cardHeader">{props.header}</div>
        <div>{props.children}</div>
      </div>
    );
  };

  global.Card = Card;
})(window);
