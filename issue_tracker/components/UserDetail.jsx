const { React } = window;

(global => {
  /**
   * This component shows a circular image with the name and email of the user.
   * It is used in the view of a single issue.
   * A presentational component.
   */
  const userDetailContainer = {
    marginTop: 10,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  };

  const imageStyle = {
    height: 50,
    width: 50,
    borderRadius: 40
  };

  const userInformationStyle = {
    marginLeft: 10,
    display: "flex",
    flexDirection: "column"
  };

  const UserDetail = ({ user }) => {
    const { name, avatarUrl, email } = user;
    return (
      <div style={userDetailContainer}>
        <img src={avatarUrl} alt="User Avatar" style={imageStyle} />
        <div style={userInformationStyle}>
          <p>
            Assigned to {name} <br />
            <a href={`mailto:${email}`}>{email}</a>
          </p>
        </div>
      </div>
    );
  };

  global.UserDetail = UserDetail;
})(window);
