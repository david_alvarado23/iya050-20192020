const { React } = window;

(global => {
  /**
   * This component shows a circular image with the name and email of the user.
   * It is used in the view of a single issue.
   * A presentational component.
   */
  const commentContainer = {
    marginTop: 10,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  };

  const imageStyle = {
    height: 50,
    width: 50,
    borderRadius: 40
  };

  const commentStyle = {
    flex: 0.9,
    marginLeft: 10,
    display: "flex",
    flexDirection: "column"
  };

  const Comment = ({ message, user, createdAt }) => {
    const { name, avatarUrl, email } = user;
    return (
      <div style={commentContainer}>
        <img
          src={avatarUrl}
          alt="User Avatar"
          style={imageStyle}
          title={email}
        />
        <div style={commentStyle}>
          <p>
            <b>{name}</b> at {new Date(createdAt).toLocaleString()}
          </p>
          <p style={{ marginTop: 0 }}>{message}</p>
        </div>
      </div>
    );
  };

  global.Comment = Comment;
})(window);
