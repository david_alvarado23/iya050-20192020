const { React, Card, IssueList } = window;

const cardHeaderStyles = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between"
};

(global => {
  /**
   * The principal view where is rendered the issues list,
   * the title and the create issue button.
   * Makes the initial request to get all the issues.
   * @param {*} props
   */
  const Home = props => {
    const getIssueStatusStyle = statusCode => {
      return statusCode === "IN_PROGRESS"
        ? { backgroundColor: "#00b4db", color: "white" }
        : statusCode === "DONE"
        ? { backgroundColor: "#21ba45", color: "white" }
        : {};
    };

    return (
      <div className="container">
        <div className="containerHeader">
          <h1 className="title">Issues</h1>
          <button
            className="button"
            onClick={() => props.history.push(`/create/issue`)}
          >
            Create issue
          </button>
        </div>
        <IssueList>
          {props.issues.map(({ id, name, status, description }) => {
            const {
              status: statusCode,
              description: statusDescription
            } = status;

            const header = (
              <div style={cardHeaderStyles}>
                <h4 style={{ margin: 0 }}>{name}</h4>
                <label
                  className="label"
                  style={getIssueStatusStyle(statusCode)}
                >
                  {statusDescription}
                </label>
              </div>
            );

            return (
              <Card
                key={id}
                header={header}
                clickeable
                onClick={() => props.history.push(`/issue/${id}`)}
              >
                <p>{description}</p>
              </Card>
            );
          })}
        </IssueList>
      </div>
    );
  };

  global.Home = Home;
})(window);
