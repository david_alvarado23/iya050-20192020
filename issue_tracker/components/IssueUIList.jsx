const { React } = window;

const ListStyle = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginLeft: -10
};

const ListItemStyle = {
  display: "inline-block",
  flexGrow: 1,
  marginTop: 15,
  marginLeft: 15,
  width: "calc(100% * (1/2) - 15px - 1px)"
};

(global => {
  /**
   * This is a wrapper to add style to a list and its children keeping the children styles.
   * @param {*} props
   */
  const IssueList = props => {
    return (
      <div style={ListStyle}>
        {React.Children.map(props.children, child => (
          <div style={{ ...ListItemStyle, ...child.props.style }}>{child}</div>
        ))}
      </div>
    );
  };

  global.IssueList = IssueList;
})(window);
