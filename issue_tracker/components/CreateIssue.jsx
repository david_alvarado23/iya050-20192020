const { React, Input, TextArea, Select } = window;
const { useEffect, useState } = React;

(global => {
  const defaultNewIssue = {
    type: "1",
    name: "",
    description: "",
    status: "1",
    asignedTo: "1"
  };

  const requiredFields = ["type", "name", "status", "asignedTo"];

  /**
   * The form to create a new issue. It takes the FormFields to keep the UI.
   * Make some initial request to get the status, users, and issue types.
   * The form is validated from an array of names. If the name is in the array, then the field is required.
   * @param {*} props
   */
  const CreateIssue = props => {
    const [newIssue, setNewIssue] = useState(defaultNewIssue);
    const [statusList, setStatusList] = useState([]);
    const [users, setUsers] = useState([]);
    const [issueTypes, setIssueTypes] = useState([]);
    const [errorMessage, setErrorMessage] = useState();

    useEffect(() => {
      getStatus();
      getUsers();
      getIssuesType();
    }, []);

    const getStatus = () => {
      fetch("http://localhost:8080/stub.json")
        .then(res => res.json())
        .then(parsedRes => parsedRes.status)
        .then(setStatusList);
    };

    const getUsers = () => {
      fetch("http://localhost:8080/stub.json")
        .then(res => res.json())
        .then(parsedRes => parsedRes.users)
        .then(setUsers);
    };

    const getIssuesType = () => {
      fetch("http://localhost:8080/stub.json")
        .then(res => res.json())
        .then(parsedRes => parsedRes.types)
        .then(setIssueTypes);
    };

    const handleFormField = event => {
      const { value, name } = event.target;

      setNewIssue(prevNewIssue => ({
        ...prevNewIssue,
        [name]: value
      }));
    };

    const checkValidation = () => {
      const invalidFields = [];
      const isValid = Object.entries(newIssue).every(([key, value]) => {
        if (requiredFields.includes(key)) {
          const isFieldValid = !!value;
          if (!isFieldValid) {
            invalidFields.push(key);
          }

          return isFieldValid;
        }

        return true;
      });

      return {
        isValid,
        invalidFields
      };
    };

    const handleSubmit = e => {
      e.preventDefault();

      const { isValid, invalidFields } = checkValidation();
      if (!isValid) {
        setErrorMessage(
          `The following fields are required: ${invalidFields.join(", ")}`
        );
        return;
      }

      fetch("http://localhost:8080/stub.json", {
        method: "POST",
        body: JSON.stringify(newIssue)
      })
        .then(res => res.json())
        .then(({ id }) => props.history.push(`/issue/${id}`))
        .catch(() => setErrorMessage("The issue couldn't be created."));
    };

    const { type, name, description, status, asignedTo } = newIssue;

    return (
      <div className="container">
        <div className="backButton" onClick={props.history.goBack}>
          &larr;
        </div>
        <div className="containerHeader">
          <h1 className="subtitle">Create an Issue</h1>
        </div>
        {errorMessage && <div className="errorMessage">{errorMessage}</div>}
        <div style={{ marginTop: 20 }}>
          <form>
            <Select
              title="TYPE"
              name="type"
              options={issueTypes.map(issueType => ({
                ...issueType,
                value: issueType.type
              }))}
              value={type}
              onChange={handleFormField}
            />
            <Input
              title="NAME"
              name="name"
              value={name}
              onChange={handleFormField}
            />
            <TextArea
              title="DESCRIPTION"
              name="description"
              value={description}
              onChange={handleFormField}
            />
            <Select
              title="STATUS"
              name="status"
              options={statusList.map(status => ({
                ...status,
                value: status.description
              }))}
              value={status}
              onChange={handleFormField}
            />
            <Select
              title="ASIGNEE TO"
              name="asignedTo"
              options={users.map(user => ({
                ...user,
                value: user.name
              }))}
              value={asignedTo}
              onChange={handleFormField}
            />
            <button
              className="button"
              style={{ float: "right" }}
              onClick={handleSubmit}
            >
              Create
            </button>
          </form>
        </div>
      </div>
    );
  };

  global.CreateIssue = CreateIssue;
})(window);
