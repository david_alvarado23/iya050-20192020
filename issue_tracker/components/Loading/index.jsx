const { React } = window;

(global => {
  /**
   * A loading component from
   * https://loading.io/css/
   * Just turned it to a React component and made a css file with the css given.
   * A Presetational component.
   */
  const Loading = () => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column"
        }}
      >
        <div className="lds-ring">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <p>Loading</p>
      </div>
    );
  };

  global.Loading = Loading;
})(window);
