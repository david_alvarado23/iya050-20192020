const { React, Loading, RightPanel, LeftPanel } = window;
const { useEffect, useState } = React;

(global => {
  /**
   * The view of a concrete and single issue. It request a issue given an id in the url.
   * This is the main container since its divided in right and left panel.
   * The left one shows the description and its comments.
   * The right one shows the issue status, to whom its assigned and the creation date.
   * @param {*} props
   */
  const Issue = props => {
    const [issue, setIssue] = useState();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
      getData();
    }, []);

    const getData = () => {
      const issueFound = props.issues.find(
        issue => issue.id === +props.match.params.id
      );
      setIssue(issueFound);
      setLoading(false);
    };

    if (loading) {
      return (
        <div style={{ marginTop: "3%" }}>
          <Loading />
        </div>
      );
    }

    return (
      <div className="container">
        <div className="backButton" onClick={props.history.goBack}>
          &larr;
        </div>
        <div className="containerHeader">
          <h1 className="subtitle">{issue.name}</h1>
        </div>
        <div style={{ display: "flex", flexDirection: "row", marginTop: 20 }}>
          <LeftPanel
            description={issue.description}
            comments={issue.comments}
          />
          <RightPanel
            issue={issue}
            onChange={selectedStatus => {
              props.changeStatus(issue.id, selectedStatus);
            }}
          />
        </div>
      </div>
    );
  };

  global.Issue = Issue;
})(window);
