const { React, Comment } = window;

(global => {
  const LeftPanel = ({ description, comments }) => {
    return (
      <div style={{ width: "75%" }}>
        <section className="section">
          <label className="subHeader">DESCRIPTION</label>
          <p>{description}</p>
        </section>
        <section className="section">
          <label className="subHeader">COMMENTS</label>
          {comments &&
            comments.map(({ id, comment: message, ...rest }) => (
              <React.Fragment key={id}>
                <Comment message={message} {...rest} />
                <hr style={{ border: "0.5px solid lightgray" }} />
              </React.Fragment>
            ))}
        </section>
      </div>
    );
  };

  global.LeftPanel = LeftPanel;
})(window);
