const { React, UserDetail, Select } = window;
const { useEffect, useState } = React;

(global => {
  const RightPanel = ({ issue, onChange }) => {
    const [status, setStatus] = useState([]);

    useEffect(() => {
      getStatus();
    }, []);

    const getStatus = () => {
      fetch("http://localhost:8080/stub.json")
        .then(res => res.json())
        .then(parsedRes => parsedRes.status)
        .then(setStatus);
    };

    const getIssueStatusStyle = statusCode => {
      return statusCode === "IN_PROGRESS"
        ? { backgroundColor: "#00b4db", color: "white" }
        : statusCode === "DONE"
        ? { backgroundColor: "#21ba45", color: "white" }
        : { backgroundColor: "#e8e8e8", color: "rgba(0, 0, 0, 0.6)" };
    };

    return (
      <div style={{ width: "25%" }}>
        <section className="section">
          <Select
            title="STATUS"
            className="selectStatus"
            style={{
              ...getIssueStatusStyle(issue.status.status),
              width: "100%"
            }}
            onChange={e =>
              onChange(status.find(status => status.id === +e.target.value))
            }
            options={status.map(status => ({
              ...status,
              value: status.description
            }))}
            optionProps={option => ({
              style: getIssueStatusStyle(option.status)
            })}
            value={issue.status.id}
          />
        </section>
        {issue.assignedToUser && (
          <section className="section">
            <label className="subHeader">ASIGNEE</label>
            <UserDetail user={issue.assignedToUser} />
          </section>
        )}
        <section className="section">
          <label className="subHeader">CREATED AT</label>
          <p>{new Date(issue.createdAt).toLocaleString()}</p>
        </section>
      </div>
    );
  };

  global.RightPanel = RightPanel;
})(window);
