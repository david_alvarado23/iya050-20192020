const http = require("http");
const fs = require("fs");
const path = require("path");
const PORT = process.env.PORT || 8080;

const mimeLookup = {
  ".js": "application/javascript",
  ".json": "application/json",
  ".jsx": "application/javascript",
  ".css": "text/css",
  ".png": "image/png",
  ".mp3": "audio/mpeg",
  ".html": "text/html",
  ".ico": "image/x-icon"
};

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    let fileurl;
    if (req.url === "/") {
      fileurl = "/public/index.html";
    } else if (req.url === "/favicon.ico") {
      fileurl = "/public/favicon.ico";
    } else {
      fileurl = req.url;
    }

    let filepath = path.resolve("./" + fileurl);

    fs.exists(filepath, exists => {
      if (exists) {
        let fileExt = path.extname(filepath);
        let mimeType = mimeLookup[fileExt];

        res.writeHead(200, { "Content-Type": mimeType });
        fs.createReadStream(filepath).pipe(res);
      } else {
        res.writeHead(404);
        res.end();
      }
    });
  } else {
    res.writeHead(404);
    res.end();
  }
});

server.listen(PORT, () => {
  console.log(`Listening to port ${PORT}`);
});
