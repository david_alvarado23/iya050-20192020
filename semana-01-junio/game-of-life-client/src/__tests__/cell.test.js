import React from "react";
import { render, fireEvent } from "@testing-library/react";
import sinon from "sinon";
import Cell from "../components/Cell";

describe("Cell Component", () => {
  test("renders component", () => {
    const { getByTestId } = render(<Cell />);
    const cell = getByTestId("cell");
    expect(cell).toBeDefined();
  });

  test("renders the correct color when is selected", () => {
    const { getByTestId } = render(<Cell selected={true} />);
    const cell = getByTestId("cell");

    expect(cell.style.backgroundColor).toBe("floralwhite");
  });

  test("renders the correct color when not selected", () => {
    const { getByTestId } = render(<Cell selected={false} />);
    const cell = getByTestId("cell");

    expect(cell.style.backgroundColor).toBe("rgb(195, 55, 55)");
  });

  test("renders with the correct size", () => {
    const cellSize = 10;
    const { getByTestId } = render(<Cell selected={true} size={cellSize} />);
    const cell = getByTestId("cell");

    expect(cell.style.height).toBe(`${cellSize}px`);
    expect(cell.style.width).toBe(`${cellSize}px`);
  });

  test("click method has been called once", () => {
    const stubFunction = sinon.stub();

    const { getByTestId } = render(<Cell onClick={stubFunction} />);
    const cell = getByTestId("cell");

    fireEvent.click(cell);
    expect(stubFunction.calledOnce);
  });
});
