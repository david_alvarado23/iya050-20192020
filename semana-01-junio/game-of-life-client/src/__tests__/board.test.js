import React from "react";
import { render } from "@testing-library/react";
import Board from "../components/Board";

describe("Board Component", () => {
  test("renders component", () => {
    const boardArray = [
      [0, 0],
      [0, 0],
    ];

    const { getByTestId } = render(<Board board={boardArray} />);
    const board = getByTestId("board");

    expect(board).toBeDefined();
  });

  test("renders with correct size", () => {
    const boardArray = [
      [0, 0],
      [0, 0],
    ];

    // Size of each board row is 50px
    const cellSize = 50;
    const boardSize = cellSize * boardArray.length;

    const { getByTestId } = render(<Board board={boardArray} />);
    const board = getByTestId("board");

    expect(board.style.height).toBe(`${boardSize}px`);
    expect(board.style.width).toBe(`${boardSize}px`);
  });

  test("has the correct number of rows", () => {
    const boardArray = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0],
    ];

    const { getByTestId } = render(<Board board={boardArray} />);
    const board = getByTestId("board");

    expect(board.querySelectorAll(".boardRow").length).toBe(boardArray.length);
  });

  test("has the correct number of cells", () => {
    const boardArray = [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
    ];

    const { getByTestId } = render(<Board board={boardArray} />);
    const board = getByTestId("board");

    const boardCellsLength = boardArray.flat().length;
    expect(board.querySelectorAll(".cell").length).toBe(boardCellsLength);
  });

  test("has the correct number of selected cells", () => {
    const boardArray = [
      [0, 0, 0, 0],
      [0, 1, 1, 0],
      [0, 1, 1, 0],
      [0, 0, 0, 0],
    ];

    const { getByTestId } = render(<Board board={boardArray} />);
    const board = getByTestId("board");

    const boardCellsLength = boardArray.flat().filter((cell) => !!cell).length;
    const cells = [...board.querySelectorAll(".cell")];
    const cellsSelected = cells.filter(
      (cell) => cell.style.backgroundColor === "floralwhite"
    ).length;

    expect(cellsSelected).toBe(boardCellsLength);
  });
});
