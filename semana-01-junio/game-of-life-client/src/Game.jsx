import React, { useState, useEffect } from "react";
import Board from "./components/Board";
import Button from "./components/Button";
import { setInitalState, getNextState } from "./apis";

// Game's states
const STARTED = "STARTED";
const AUTO = "AUTO";
const STOPPED = "STOPPED";

// Some variables that don't need to be updated by React.
const squaresPerSide = 10;
const initialBoard = new Array(squaresPerSide).fill(
  new Array(squaresPerSide).fill(0)
);
let timer;

const Game = () => {
  const [gameState, setGameState] = useState();
  const [hasGameStarted, setHasGameStarted] = useState(false);
  const [board, setBoard] = useState(initialBoard);
  const [hasBoardChangeManually, setHasBoardChangeManually] = useState(false);

  useEffect(() => {
    setHasGameStarted(
      gameState === STARTED || gameState === AUTO || gameState === STOPPED
    );
  }, [gameState]);

  const onCellClick = (x, y) => {
    setBoard((prevBoard) => [
      ...prevBoard.slice(0, y),
      [
        ...prevBoard[y].slice(0, x),
        prevBoard[y][x] === 0 ? 1 : 0,
        ...prevBoard[y].slice(x + 1, prevBoard[y].length),
      ],
      ...prevBoard.slice(y + 1, prevBoard.length),
    ]);
    setHasBoardChangeManually(true);
  };

  const boardIsEmpty = () => board.flat().every((cell) => !cell);

  const updateGameStatus = () => {
    if (!hasGameStarted) {
      if (boardIsEmpty()) {
        alert("The board must have an initial state.");
        return;
      }

      sendInitialState();
    } else {
      clearInterval(timer);
      setGameState(null);
      setBoard(initialBoard);
    }
  };

  const sendInitialState = async () => {
    setGameState(STARTED);
    const newBoard = await setInitalState(board);
    setBoard(newBoard);
    setHasBoardChangeManually(false);
  };

  const loadNextState = async () => {
    if (hasBoardChangeManually) {
      await setInitalState(board);
    }

    const newBoard = await getNextState();
    setBoard(newBoard);
    setHasBoardChangeManually(false);
  };

  const updateAutoInterval = () => {
    if (gameState === AUTO) {
      setGameState(STOPPED);
      clearInterval(timer);
    } else {
      setGameState(AUTO);

      const tick = 1500; // Each 1,5 secs
      timer = setInterval(() => {
        loadNextState();
      }, tick);
    }
  };

  return (
    <>
      <nav className="navBar">
        <div className="title">Conway's Game of Life</div>
      </nav>
      <div className="container">
        <Board board={board} onCellClick={onCellClick} />
        <div className="buttonContainer">
          <Button id="startButton" onClick={updateGameStatus}>
            {!hasGameStarted ? "Start Game" : "Reset"}
          </Button>
          {hasGameStarted && (
            <>
              <Button id="nextButton" onClick={loadNextState}>
                Next
              </Button>
              <Button id="clockButton" onClick={updateAutoInterval}>
                {gameState !== AUTO ? "Start" : "Stop"}
              </Button>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Game;
