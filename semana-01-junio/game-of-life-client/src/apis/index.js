const apiUrl = "http://localhost:8080";
const commonHeaders = {
  "Content-Type": "application/json",
};

export const setInitalState = async (board) => {
  const response = await fetch(`${apiUrl}/life`, {
    method: "POST",
    headers: commonHeaders,
    body: JSON.stringify(board),
  });
  return await response.json();
};

export const getNextState = async () => {
  const response = await fetch(`${apiUrl}/life/next`, {
    method: "POST",
    headers: commonHeaders,
  });
  return await response.json();
};
