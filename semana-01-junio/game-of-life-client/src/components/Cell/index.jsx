import React from "react";
import "./index.css";

const Cell = (props) => {
  return (
    <div
      data-testid="cell"
      className="cell"
      onClick={props.onClick}
      style={{
        backgroundColor: props.selected ? "floralwhite" : "rgb(195, 55, 55)",
        width: props.size,
        height: props.size,
      }}
    />
  );
};

export default Cell;
