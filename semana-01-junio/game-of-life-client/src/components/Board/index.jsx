import React from "react";
import "./index.css";
import Cell from "../Cell";

const cellSize = 50;

const Board = (props) => {
  const sizeOfBoard = props.board.length * cellSize;
  const style = { height: sizeOfBoard, width: sizeOfBoard };

  return (
    <div className="board" data-testid="board" style={style}>
      {props.board.map((row, y) => (
        <div className="boardRow" key={y}>
          {row.map((cell, x) => (
            <Cell
              size={cellSize}
              selected={!!cell}
              onClick={() => props.onCellClick(x, y)}
              key={x}
            />
          ))}
        </div>
      ))}
    </div>
  );
};

export default Board;
