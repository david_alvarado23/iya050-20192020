describe("Game Tests", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("shows an alert since any cell has been selected", () => {
    cy.get("#startButton").click();
    cy.on("window:alert", (text) => {
      expect(text).to.equal("The board must have an initial state.");
    });
  });

  it("clicks a cell and change of color", () => {
    cy.get(".cell")
      .first()
      .as("firstCell");

    cy.get("@firstCell").click();

    cy.get("@firstCell").should(
      "have.css",
      "background-color",
      "rgb(255, 250, 240)"
    );
  });

  it("clicks a cell twice", () => {
    cy.get(".cell")
      .first()
      .as("firstCell");

    cy.get("@firstCell").click();
    cy.get("@firstCell").should(
      "have.css",
      "background-color",
      "rgb(255, 250, 240)"
    );

    cy.get("@firstCell").click();
    cy.get("@firstCell").should(
      "have.css",
      "background-color",
      "rgb(195, 55, 55)"
    );
  });

  it("starts the game", () => {
    cy.get(".boardRow:nth-child(5) > .cell").click({ multiple: true });

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });

    cy.get("#startButton").click();
    cy.get("#nextButton").click();
    // To make the request
    cy.wait(1000);

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });
  });

  it("reset the cells", () => {
    cy.get(".boardRow:nth-child(2) > .cell").click({ multiple: true });

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });

    cy.get("#startButton").click();
    cy.get("#nextButton").click();

    // The reset button is the same as the start button
    cy.get("#startButton").click();
    cy.contains("Start Game");

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });
  });

  it("starts the auto mode", () => {
    cy.get(".boardRow:nth-child(6) > .cell").click({ multiple: true });
    cy.get(".boardRow:nth-child(4) > .cell").click({ multiple: true });

    cy.get("#startButton").click();
    cy.get("#clockButton").click();
    cy.wait(5000);
    // Stop the auto mode
    cy.get("#clockButton").click();

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });
  });

  it("changes the cells after clicking next", () => {
    cy.get(".boardRow:nth-child(2) > .cell").click({ multiple: true });

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });

    cy.get("#startButton").click();
    cy.get("#nextButton").click();
    // To make the request
    cy.wait(1000);

    cy.get(".boardRow:nth-child(6) > .cell").click({ multiple: true });
    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });

    cy.get("#nextButton").click();
    // To make the request
    cy.wait(1000);

    cy.get(".board").toMatchImageSnapshot({ threshold: 0.05 });
  });
});
